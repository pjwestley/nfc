/**************************************************************************/
/*! 
    This example will wait for any ISO14443A card or tag, and
    depending on the size of the UID will attempt to read from it.
   
    If the card has a 4-byte UID it is probably a Mifare
    Classic card, and the following steps are taken:
   
    - Authenticate block 4 (the first block of Sector 1) using
      the default KEYA of 0XFF 0XFF 0XFF 0XFF 0XFF 0XFF
    - If authentication succeeds, we can then read any of the
      4 blocks in that sector (though only block 4 is read here)
	 
    If the card has a 7-byte UID it is probably a Mifare
    Ultralight card, and the 4 byte pages can be read directly.
    Page 4 is read by default since this is the first 'general-
    purpose' page on the tags.

    To enable debug message, define DEBUG in PN532/PN532_debug.h
    *************************************************************
     Black: GND
     White: +5V/Vin
      Grey: MOSI/A5
    Purple: MISO/A4
      Blue: SCK/A3
     Green: CS0/D0
    Yellow: CS1/D1
    Orange: CS2/D2
       Red: CS3/D4
     Brown: CS4/D5
*/
/**************************************************************************/


// Particle stuffery
#include <Particle.h>
#define LED D7
#define BEEPER D3
#define MODE A7

// mDNS Setup
#include <MDNS.h>
#include <vector>

#define HTTP_PORT 80
#define ALT_HTTP_PORT 8080

MDNS mdns;

TCPServer server = TCPServer(HTTP_PORT);
TCPServer altServer = TCPServer(ALT_HTTP_PORT);

// publish setup
void deviceNameHandler(const char *topic, const char *data);
const char *PUBLISH_EVENT_NAME = "tag-scan-data";
String deviceName = "";

// NFC Readers setup
// Total number of NFC readers to try and look for
#define NUM_READERS 5
#if NUM_READERS > 7
  #error NUM_READERS Cannot be more than 7
#endif
#if 1
  #include <SPI.h>
  #include <PN532_SPI.h>
  #include "PN532.h"

  PN532_SPI reader_specs[NUM_READERS]{
    PN532_SPI(SPI,D0),
    PN532_SPI(SPI,D1),
    PN532_SPI(SPI,D2),
    PN532_SPI(SPI,D4),
    PN532_SPI(SPI,D5)
    };
  PN532 reader[NUM_READERS]{
    PN532(reader_specs[0]),
    PN532(reader_specs[1]),
    PN532(reader_specs[2]),
    PN532(reader_specs[3]),
    PN532(reader_specs[4])
  };
#elif 0
  #include <PN532_HSU.h>
  #include <PN532.h>
      
  PN532_HSU pn532hsu(Serial1);
  PN532 nfc(pn532hsu);
#else 
  #include <Wire.h>
  #include <PN532_I2C.h>
  #include <PN532.h>
  PN532_I2C pn532i2c(Wire);
  PN532 nfc(pn532i2c);	
#endif

// Make sure antenna selection is optimal
STARTUP(WiFi.selectAntenna(ANT_AUTO);)

void setup(void) {
  Serial.begin(115200);
  Serial.println("Hello!");

  // NFC reader detection and read indicator
  pinMode(LED,OUTPUT);
  digitalWrite(LED,LOW);

  // Beeper
  pinMode(BEEPER,OUTPUT);
  digitalWrite(BEEPER,LOW);

  // Mode input switch
  pinMode(MODE,INPUT_PULLUP);

  // Pub/Sub Event stuffery to get my name
  // name is used as source in data collection
  Particle.subscribe("spark/", deviceNameHandler,MY_DEVICES);
	Particle.publish("spark/device/name",PRIVATE);

  // Intialise reader objects
  for(int i = 0; i < NUM_READERS; i++) {
    // reader_specs[i] = PN532_SPI(SPI, D0+i);
    reader[i] = PN532(reader_specs[i]);
  }

  // Initialse all readers, try if they are there or not
  uint32_t versiondata[NUM_READERS] = {0};
  for (int r=0;r<NUM_READERS;r++){
    reader[r].begin();

    // Try 3 times to see if reader is responding
    Serial.printf("trying %d: ",r);
    for (int t=0;(! versiondata[r]) && (t < 3);t++ ) {
      versiondata[r] = reader[r].getFirmwareVersion();
      if(! versiondata[r])
        Serial.print(".");
    }
    if(versiondata[r]) {
      // Got ok data, print it out!
      Serial.print("Found PN5"); Serial.print((versiondata[r]>>24) & 0xFF, HEX); 
      Serial.print(" v"); Serial.print((versiondata[r]>>16) & 0xFF, DEC); 
      Serial.print('.'); Serial.println((versiondata[r]>>8) & 0xFF, DEC);
      // Serial.printlnf(" at %d",r);

      // Flash LED twice
      digitalWrite(LED,HIGH);
      delay(200);
      digitalWrite(LED,LOW);
      // delay(100);
      // digitalWrite(LED,HIGH);
      // delay(200);
      // digitalWrite(LED,LOW);
      delay(400);

      // configure board to read RFID tags
      reader[r].SAMConfig();
    }
    else 
    {
      Serial.println(" not found.");
    }
  }
  // Wait for my device name
  while(deviceName == ""){
    Particle.process();
  }

  beep();
  Serial.print(String::format("%s Ready.\n",deviceName.c_str()));
}

void beep(){
  digitalWrite(BEEPER,HIGH);
  delay(75);
  digitalWrite(BEEPER,LOW);
  delay(100);
  digitalWrite(BEEPER,HIGH);
  delay(75);
  digitalWrite(BEEPER,LOW);
}

void loop(void) {
  uint8_t success;
  uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
  bool mode = digitalRead(MODE);

  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  for(int r = 0; r < NUM_READERS;r++) {
    success = reader[r].readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength,100);

    if (success) {
      // Display some basic information about the card
      Serial.print(String::format("%d : ",r));
      reader[r].PrintHex(uid, uidLength);
      Serial.println("");

      // Flash LED
      digitalWrite(LED,HIGH);
      delay(200);
      digitalWrite(LED,LOW);
      beep();

      char t[14];
      char tt[22];
      if (uidLength == 4)
      {
        snprintf(t,sizeof(t),"%02X%02X%02X%02X",uid[0],uid[1],uid[2],uid[3]);
        snprintf(tt,sizeof(tt),"Tag Mifare Classic");
      }
      if (uidLength == 7)
      {
        snprintf(t,sizeof(t),"%02X%02X%02X%02X%02X%02X%02X",uid[0],uid[1],uid[2],uid[3],uid[4],uid[5],uid[6]);
        snprintf(tt,sizeof(tt),"Tag Mifare Ultralight");
      }

      // Publishery
      char buf[256];
      snprintf(buf, sizeof(buf), "{\"tag\":\"%s\",\"tag-type\":\"%s\",\"reader\":%d,\"source\":\"%s\",\"mode\":\"%s\"}", t, tt, r, deviceName.c_str(), mode?"stars":"entry");
      Serial.printlnf("publishing %s", buf);
      Particle.publish(PUBLISH_EVENT_NAME, buf, PRIVATE);
    }
  }
}

void deviceNameHandler(const char *topic, const char *data) {
	deviceName = data;
}